# Perun
Perun is slavic god of sky, thunder, storms, rain .. (god of weather) [https://en.wikipedia.org/wiki/Perun]

The goal of the application is to receive data from different weather stations that send data in different file format, different units and even different amount of data.

The first challenge that I met is how to handle different units (metric and imperial). I decided to have some general config for units and store weather stations data in that unit.
Let's imagine that there is general settings in the application caled *units* abd based on that upon the import we convert the data in that unit.

For example: if unit is set to be metric we convert imperial data from stations to metric and vice versa. Implementing this strategy will easier displaying and calculating data from different stations.

The response is returned based on the unit setting again.

One of the improvements that I see here is if users are able to select each field in which unit they want to see it. 

Another improvement would be reading the files would be handled by Jobs (in Laravel) or workers / queues. These task/jobs would be triggered by cronjob.


After import is done it would be nice if notification is sent (somewhere).

Entry point for the application is **app/Services/Perun/ImportService::import**

To add new station:
1. Add new record in **weather_stations** table with needed configuration
   1. Name
   2. Address (address_id) 
   3. Units (imperial or metric)
   4. status
   5. Source type (json, csv, xml ...)
2. Implement **app/Services/Perun/Import/Importers/ImporterInterface.php** and  **app/Services/Perun/Import/Readers/ReaderInterface.php**

3. To return data either for internal or external api **app/Services/Perun/WeatherStationService.php** is used.

Note: code is not tested and may not be working if it's deployed as it is.
The goal was to implement the idea. 
