<?php

namespace App\Providers;

use App\Repositories\Contracts\WeatherDataInterface;
use App\Repositories\Contracts\WeatherStationInterface;
use App\Repositories\WeatherDataRepository;
use App\Repositories\WeatherStationRepository;
use Illuminate\Support\ServiceProvider;

class RepositoriesServiceProvider extends ServiceProvider
{

    protected $repositories = [
        WeatherStationInterface::class => WeatherStationRepository::class,
        WeatherDataInterface::class => WeatherDataRepository::class
    ];
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        foreach ($this->repositories as $interface => $repository) {
            $this->app->bind($interface, function ($app) use ($repository) {
                return new $repository;
            });
        }
    }


    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
