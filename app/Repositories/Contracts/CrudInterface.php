<?php

namespace App\Repositories\Contracts;

use Illuminate\Database\Eloquent\Model;

interface CrudInterface
{
    public function store(array $data): Model;

    public function update(int $id, array $data): Model;

    public function delete(int $id): void;
}
