<?php

namespace App\Repositories\Contracts;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

interface WeatherDataInterface
{
    public function insert(array $data): Model;

    public function getDataByTimeAndStation(\DateTime $time, int $stationId): Collection;

    public function getAvgDataForActiveStationsByTime(\DateTime $time): Collection;

    public function getLastAvailable(): Model;

}
