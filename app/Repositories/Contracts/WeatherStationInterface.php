<?php

namespace App\Repositories\Contracts;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

interface WeatherStationInterface
{
    public function findByName(string $name): Model;

    public function getActiveStations(): Collection;
}
