<?php

namespace App\Repositories;

use App\Repositories\Contracts\CrudInterface;
use Illuminate\Database\Eloquent\Model;

class CrudRepository implements CrudInterface
{
    protected string $model;

    /**
     * @param array $data
     * @return Model
     */
    public function store(array $data): Model
    {
        return $this->model::create($data);
    }

    /**
     * @param int $id
     * @param array $data
     * @return Model
     */
    public function update(int $id, array $data): Model
    {
        $model = $this->model::find($id);

        $model->fill($data);
        $model->save();

        return  $model;
    }

    public function delete(int $id): void
    {
        $this->model::destroy($id);
    }
}
