<?php

namespace App\Repositories;

use App\Models\WeatherData;
use App\Repositories\Contracts\WeatherStationInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

class WeatherStationRepository extends CrudRepository implements WeatherStationInterface
{
    protected string $model = WeatherData::class;

    /**
     * @param string $name
     * @return mixed
     */
    public function findByName(string $name): Model
    {
        return $this->model::where(['name' => $name])->get()->first();
    }

    public function getActiveStations(): Collection
    {
        return $this->model::where(['status' => 1])->get();
    }

}
