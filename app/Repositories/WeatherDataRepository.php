<?php

namespace App\Repositories;

use App\Models\WeatherData;
use App\Repositories\Contracts\WeatherDataInterface;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class WeatherDataRepository extends CrudRepository implements WeatherDataInterface
{
    protected string $model = WeatherData::class;

    public function insert(array $data): Model
    {
        return $this->model::insert($data);
    }

    public function getDataByTimeAndStation(\DateTime $time, ?int $stationId): Collection
    {
        $query = $this->model::query();

        $query->where(['time' => $time]);

        if (! is_null($stationId)) {
            $query->where(['id' => $stationId]);
        }

        return $query->get();
    }


    public function getAvgDataForActiveStationsByTime(\DateTime $date): Collection
    {
        $query = $this->model::query();

        $query->where(['time' => $date]);
        $query->where(['status' => 1]);

        $query->avg('temperature');
        $query->avg('humidity');
        $query->avg('wind');

        return $query->get();
    }


    public function getLastAvailable(): Model
    {
        return $this->model::latest()->take(1);
    }

}
