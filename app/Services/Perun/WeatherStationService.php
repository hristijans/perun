<?php

namespace App\Services\Perun;

use App\Repositories\Contracts\WeatherDataInterface;
use App\Services\Perun\UnitConverters\ConvertItem;

class WeatherStationService
{
    public function __construct(
        protected WeatherDataInterface $weatherData,
    )
    {

    }


    public function getDataByDateAndStation(\DateTime $date, int $stationId): array
    {
        $response = $this->weatherData->getDataByTimeAndStation($date, $stationId);
        $data = [];

        foreach ($response as  $item) {
            $data[] = $data[] = (new ConvertItem())->convert($item);
        }

        return $data;
    }


    public function getAvgDataForActiveStationsAndTime(\DateTime $date): array
    {
        $response = $this->weatherData->getAvgDataForActiveStationsByTime($date);
        $data = [];

        foreach ($response as  $item) {
            $data[] = (new ConvertItem())->convert($item);
        }

        return $data;
    }

    public function getLast(): array
    {
        $response = $this->weatherData->getLastAvailable();
        $data = [];

        foreach ($response as  $item) {
            $data[] = (new ConvertItem())->convert($item);
        }

        return $data;
    }

}
