<?php

namespace App\Services\Perun\Fields;

class FieldConverter
{
    public function __construct(protected FieldInterface $field)
    {
    }

    public function convert()
    {
        return $this->field->convert();
    }

}
