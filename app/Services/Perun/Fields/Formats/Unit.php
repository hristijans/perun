<?php

namespace App\Services\Perun\Fields\Formats;

class Unit
{
    const IMPERIAL = 'imperial';
    const METRIC = 'metric';
}
