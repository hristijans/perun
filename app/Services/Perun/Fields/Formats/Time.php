<?php

namespace App\Services\Perun\Fields\Formats;

class Time
{
    const TIMESTAMP = 'timestamp';
    const DATETIME = 'datetime';
}
