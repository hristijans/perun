<?php

namespace App\Services\Perun\Fields\Fields;

use App\Services\Perun\Fields\FieldInterface;
use App\Services\Perun\Fields\Formats\Unit as UnitFormat;

class Temperature  extends  AbstractField implements FieldInterface
{

    public function convert(): mixed
    {
        if ($this->from == UnitFormat::IMPERIAL && $this->to == UnitFormat::METRIC) {
            return $this->fromImperialToMetric();
        }

        if ($this->from == UnitFormat::METRIC && $this->to == UnitFormat::IMPERIAL) {
            return $this->fromMetricToImperial();
        }

        return $this->fieldValue;
    }

    public function fromImperialToMetric(): int
    {
        return ($this->fieldValue - 32) * 0.5556;
    }

    public function fromMetricToImperial(): int
    {
        return ($this->fieldValue * 1.8) + 32;
    }

}
