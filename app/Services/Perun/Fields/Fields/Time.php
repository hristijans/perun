<?php

namespace App\Services\Perun\Fields\Fields;

use App\Services\Perun\Fields\FieldInterface;
use  App\Services\Perun\Fields\Formats\Time as TimeFormat;

class Time extends AbstractField implements FieldInterface
{

    public function convert(): mixed
    {
       if ($this->from == TimeFormat::DATETIME && $this->to == TimeFormat::TIMESTAMP) {
           return $this->fromDatetimeToTimestamp();
       }

       if ($this->from == TimeFormat::TIMESTAMP && $this->to = TimeFormat::DATETIME) {
           return $this->fromTimestampToDatetime();
       }

       return $this->fieldValue;
    }

    public function fromTimestampToDatetime(): string
    {
        return date('Y-m-d H:i:s', $this->fieldValue);
    }

    public function fromDatetimeToTimestamp(): int
    {
        $date = new \DateTime($this->fieldValue);
        return $date->getTimestamp();
    }

}
