<?php

namespace App\Services\Perun\Fields\Fields;

abstract class AbstractField
{
    protected string $from;
    protected string $to;

    public function __construct(protected string $fieldValue)
    {
    }

    public function from(string $convertFrom): self
    {
        $this->from = $convertFrom;
        return $this;
    }

    public function to(string $convertTo): self
    {
        $this->to = $convertTo;
        return $this;
    }
}
