<?php

namespace App\Services\Perun\Fields;


interface FieldInterface
{
    public function __construct(string $fieldValue);

    public function convert(): mixed;

    public function from(string $convertFrom): self;

    public function to(string $convertTo): self;
}
