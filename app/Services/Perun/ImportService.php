<?php

namespace App\Services\Perun;

use App\Repositories\Contracts\WeatherDataInterface;
use App\Repositories\Contracts\WeatherStationInterface;
use App\Services\Perun\Import\Importers\ImporterFactory;
use Illuminate\Support\Facades\Log;

/**
 * This service could be called from controller, job, task, worker ...
 */
class ImportService
{
    public function __construct(
        protected WeatherStationInterface $weatherStationRepository,
        protected WeatherDataInterface $weatherDataRepository
    )
    {
    }

    public function import(): void
    {
        /**
         * Get all active stations that can be importerd
         */
        $stations = $this->weatherStationRepository->getActiveStations();

        foreach ($stations as $station) {
           Log::debug("Import started for station {$station->id} ");
           $this->importStationByName($station->name);
           Log::debug("Import ended for station {$station->id} ");
        }

    }

    public function importStationByName(string $name): void
    {
        $station = $this->weatherStationRepository->findByName($name);

        /**
         * Decide the format file and units for the current station
         */
        $importer = ImporterFactory::import($station);
        $data = $importer->import();

        /**
         * Insert data in db
         */
        $this->weatherStationRepository->insert($data);

        Log::debug("Imported data for station {$station->id} ");
    }
}
