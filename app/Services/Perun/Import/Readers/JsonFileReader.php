<?php

namespace App\Services\Perun\Import\Readers;

use App\Services\Perun\Fields\Formats\Time;
use App\Services\Perun\Fields\Formats\Unit;
use App\Services\Perun\Fields\Formats\NullFormat;

class JsonFileReader implements ReaderInterface
{
    public function read(string $source): array
    {
        $data = [];

        $content = file_get_contents($source);
        $items =  json_decode($content, true);

        foreach ($items as $line) {
            $data[] = [
                'time' =>  [
                    'value' => $line['time'],
                    'format' => Time::DATETIME,
                ],
                'temperature' => [
                    'value' => $line['temperature'],
                    'format' => Unit::IMPERIAL,
                ],
                'humidity' => [
                    'value' => $line['humidity'],
                    'format' => Unit::IMPERIAL,
                ],
                'rain' => [
                    'value' => $line['rain'],
                    'format' => Unit::IMPERIAL,
                ],
                'wind' => [
                    'value' => $line['wind'],
                    'format' => Unit::IMPERIAL,
                ],
                'battery_level' => [
                    'value' => $line['battery_level'],
                    'format' => NullFormat::NULL_FORMAT
                ]
            ];
        }

        return $data;
    }
}
