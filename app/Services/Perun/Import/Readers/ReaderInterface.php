<?php

namespace App\Services\Perun\Import\Readers;

interface ReaderInterface
{
    public function read(string $source): array;
}
