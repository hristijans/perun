<?php

namespace App\Services\Perun\Import\Readers;

class CsvFileReader implements ReaderInterface
{
    public function read($source): array
    {
        $data = [];

        $CSVfp = fopen($source,"r");
        if ($CSVfp !== FALSE) {
            while (! feof($CSVfp)) {
                $data[] = fgetcsv($CSVfp, 1000, ",");
            }
        }

        return $data;
    }
}
