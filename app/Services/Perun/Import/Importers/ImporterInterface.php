<?php

namespace App\Services\Perun\Import\Importers;

use App\Services\Perun\Import\Readers\ReaderInterface;
use Illuminate\Database\Eloquent\Model;

interface ImporterInterface
{
    public function __construct(ReaderInterface $reader, Model $station);

    public function import(): array;

    public function getContents(): array;

    public function getSource(): string;
}
