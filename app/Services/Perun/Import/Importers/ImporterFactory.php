<?php

namespace App\Services\Perun\Import\Importers;

use App\Repositories\Contracts\WeatherStationInterface;
use App\Services\Perun\Import\Readers\JsonFileReader;
use App\Services\Perun\Import\Readers\XmlFileReader;
use Illuminate\Database\Eloquent\Model;


class ImporterFactory
{
    /**
     * Each weather station has settings for the file format that provides and units format
     * Here we will initialize importer/reader based on that settings
     */

    static public function import(Model $station): ImporterInterface
    {
        $reader = false;

        $reader = match ($station->source_type) {
            'xml' => new XmlFileReader,
            'json' => new JsonFileReader
            /**
             * add more readers here
             */
        };

        return new Importer($reader, $station);
    }
}
