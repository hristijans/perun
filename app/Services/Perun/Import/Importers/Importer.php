<?php

namespace App\Services\Perun\Import\Importers;

use App\Repositories\Contracts\WeatherStationInterface;
use App\Services\Perun\Import\Readers\ReaderInterface;
use App\Services\Perun\UnitConverters\ConverterInterface;
use App\Services\Perun\UnitConverters\ConvertItem;
use App\Services\Perun\UnitConverters\ConvertItemInterface;

class Importer implements ImporterInterface
{
    public function __construct(
        protected ReaderInterface $reader,
        protected WeatherStationInterface $station
    )
    {

    }

    public function import(): array
    {
        $contents =  $this->getContents();

        $data = [];

        foreach ($contents as $item) {
            $data[] = (new ConvertItem())->convert($item);
        }

        return $data;
    }

    public function getContents(): array
    {
        return $this->reader->read($this->getSource());
    }

    public function getSource(): string
    {
        return $this->station->source_path;
    }

}
