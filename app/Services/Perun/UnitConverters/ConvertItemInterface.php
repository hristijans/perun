<?php

namespace App\Services\Perun\UnitConverters;

interface ConvertItemInterface
{
    public function convert(array $data): array;
}
