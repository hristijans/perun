<?php

namespace App\Services\Perun\UnitConverters;

use App\Services\Perun\Fields\FieldConverter;
use App\Services\Perun\Fields\Fields\Temperature;
use App\Services\Perun\Fields\Fields\Time;

class ConvertItem implements ConvertItemInterface
{
    public function convert(array $item): array
    {
        $time = (new Time($item['time']['value']))
            ->from($item['time']['format'])
            ->to(env('time_format'));

        $temperature = (new Temperature($item['temperature']['value']))
            ->from($item['temperature']['format'])
            ->to(env('unit_format'));

        /**
         * Following are not implemented but should follow same implementation as Temperature
         */
        $humidity = null;
        $rain = null;
        $wind = null;

        return [
            'time'          => (new FieldConverter($time))->convert(),
            'temperature'   => (new FieldConverter($temperature))->convert(),
            'humidity'      => (new FieldConverter($humidity))->convert(),
            'rain'          => (new FieldConverter($rain))->convert(),
            'wind'          => (new FieldConverter($wind))->convert(),
            'battery_level' => $item['battery_level'],
            'light'         => $item['light']
        ];
    }
}
