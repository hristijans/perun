<?php

namespace App\Http\Controllers;

use App\Services\Perun\WeatherStationService;

class WeatherController extends Controller
{
    public function __construct(
        protected WeatherStationService $weatherStationService
    )
    {
       $this->weatherStationService = $weatherStationService;
    }

    public function index()
    {
        $today = date('Y-m-d');

        $data = [];

        $data['station1'] = $this->weatherStationService->getDataByDateAndStation($today, 1);
        $data['station2'] = $this->weatherStationService->getDataByDateAndStation($today, 2);
        $data['avgWeather'] = $this->weatherStationService->getAvgDataForActiveStationsAndTime($today);

        /*
         * Return and load data in some view
         */
    }
}
